(defproject clojuby-on-clails "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [link.szabo.mauricio/clojuby "0.0.1-SNAPSHOT"]
                 [hiccup "2.0.0-alpha1"]

                 [com.github.seancorfield/next.jdbc "1.2.731"]
                 [com.github.seancorfield/honeysql "2.1.818"]
                 [org.xerial/sqlite-jdbc "3.36.0.3"]]
                 ;
                 ; ;TESTING
                 ; [org.jruby/jruby "9.3.0.0"]]

  :main ^:skip-aot clojuby-on-clails.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all
                       :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}
             :dev {:dependencies [[check "0.2.1-SNAPSHOT"]]}})
