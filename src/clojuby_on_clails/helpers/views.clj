(ns clojuby-on-clails.helpers.views
  (:require [clojure.walk :as walk]
            [clojure.string :as str]
            [hiccup2.core :as h]
            [clojuby.core :as rb :refer [clj->rb defclass ruby rb->clj]]))

(def html-renderer
  (delay (let [module (rb/raw-eval "Rails.application.routes.url_helpers")]
           (defclass HTMLRenderer
             (include module)
             (include rb/ActionView.Helpers.UrlHelper)
             (include rb/ActionView.Helpers.FormHelper)
             (include rb/ActionView.Helpers.TextHelper)
             (include rb/ActionView.Context))
           (rb/new HTMLRenderer))))

(defn hiccup [html]
  (let [string (->> html
                    (walk/postwalk (fn [elem]
                                     (if (vector? elem)
                                       (case (first elem)
                                         :<> (seq (rest elem))
                                         :raw (map h/raw (rest elem))
                                         elem)
                                       elem)))
                    doall
                    h/html
                    str)]
    (rb/raw-send string "html_safe")))

#_
(->> html
     (walk/postwalk (fn [elem]
                      (if (vector? elem)
                        (case (first elem)
                          :<> (seq (rest elem))
                          :raw (map h/raw (rest elem))
                          elem)
                        elem)))
     ; doall
     ; h/html
     ; str
     (let [html [:raw [:raw "foobar"]]]))

(def post (ruby (. rb/Post first)))

(hiccup [:<>
         [:div "LOL"]
         [:raw "<f>" "</f>"]
         [:div "Lal"]])

(defn v [path & params]
  (let [method (-> path name (str/replace #"-" "_"))]
    (apply rb/send @html-renderer method params)))

(defn- convert-inner [ns-name inner-sym elem]
  (if (and (list? elem)
           (-> elem first qualified-symbol?)
           (-> elem first namespace (= ns-name)))
    [:raw `(rb/send ~inner-sym
                    ~(-> elem first name (str/replace #"-" "_"))
                    ~@(rest elem))]
    elem))

; (println
;  (form-for post
;            (post/label :title)
;            (post/text-field :title)))
(defmacro form-for [sym-name & body]
  (let [ns-name (name sym-name)
        inner-sym (gensym "form-")
        elems (walk/prewalk #(convert-inner ns-name inner-sym %) body)]
    `(v :form-for ~sym-name
        (rb/& (fn [~inner-sym]
                (hiccup [:<> ~@elems]))))))

; (println
;  (v :form-for post
;     (rb/& (fn [form]
;             (hiccup
;              [:raw
;               (ruby (. form label :title))])))))
;
;
; (println
;  (clojuby-on-clails.helpers.views/v
;   :form-for
;   post
;   (clojuby.core/&
;    (clojure.core/fn [form-14229]
;     (clojuby-on-clails.helpers.views/hiccup
;      [:<>
;       [:raw (clojuby.core/ruby (. form-14229 label :title))]])))))
