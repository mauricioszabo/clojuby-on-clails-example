(ns clojuby-on-clails.controllers.posts-controller
  (:require [clojuby.core :as rb :refer [clj->rb defclass ruby rb->clj]]
            [hiccup2.core :as h]
            [next.jdbc :as jdbc]
            [next.jdbc.sql :as sql]
            [honey.sql :as honey]
            [clojuby-on-clails.helpers.views :as v])
  (:import [org.jruby.runtime.builtin IRubyObject]))

(def db-conn
  (jdbc/get-datasource {:dbtype "sqlite"
                        :dbname "blog-example/db/development.sqlite3"}))

(defn- index! [self]
  (def self self)
  (rb/set-variable self "@posts" (ruby (. rb/Post all))))

(defn- parse-hiccup! [ & data]
  (-> data
      h/html
      str
      clj->rb
      (rb/raw-send "html_safe")))

(defn- hiccup! [self & data]
  (ruby (. self render {:html (apply parse-hiccup! data)
                        :layout true})))

(defn- show! [{:keys [self]}]
  (let [id (-> self rb/rb->clj :params :id)
        [post] (sql/query db-conn (honey/format {:select [:*]
                                                 :from [:posts]
                                                 :where [:= :id id]}))]
    (hiccup! self
             [:p#notice]
             [:p
              [:strong "Title: "]
              (:posts/title post)]
             [:p
              [:strong "Body: "]
              (:posts/body post)]
             (h/raw (ruby (. self link-to "Edit" (. self edit-post-path id))))
             " | "
             (h/raw (ruby (. self link-to "Back" (. self posts-path)))))))

#_
(defn- show! [{:keys [self super]}]
  (let [id (ruby (. (. self params) fetch :id))]
    (.instance_variable_set self
                            (clj->rb "@post")
                            (ruby (. rb/Post find id)))))



; (defn- form [self post]
;   (ruby (. self form-with {:model post}
;           (& (fn [form]
;                (parse-hiccup!
;                   [:div.field
;                    (h/raw (ruby (. form label :title)))
;                    " "
;                    (h/raw (ruby (. form text-field :title)))]))))))

(defn- form [self post]
  (v/form-for post
    (when-let [errors (ruby (. (. p errors) to-a))]
      [:div#error_explanation "LOL"
       [:h2 (v/v :pluralize (count errors) "error")
        "prohibited this post from being saved:"]]
      [:ul
       [:<> (for [error errors]
              [:li error])]])

    [:div.field
     (post/label :title)
     " "
     (post/text-field :title)]

    [:div.field
     (post/label :body)
     " "
     (post/text-area :body)]

    [:div.actions (post/submit)]))

(defn- edit! [{:keys [self]}]
  (let [id (-> self rb->clj :params :id)
        post (ruby (. rb/Post find id))]
    (hiccup! self
             [:h1 "Editing Post"]
             (h/raw (form self post))
             (h/raw (ruby (. self link-to "Show" (. self post-path post))))
             " | "
             (h/raw (ruby (. self link-to "Back" (. self posts-path)))))))

(defclass ^:ruby-class PostsController < rb/ApplicationController
  (index [{:keys [self]}] (index! self))
  (show [params] (show! params))
  (edit [params] (edit! params)))


(rb/add-method! PostsController "edit" #(edit! %))
(rb/eval "PostsController.include ActionView::Helpers::UrlHelper")
(rb/eval "PostsController.include ActionView::Helpers::FormHelper")
(rb/eval "PostsController.include ActionView::Helpers::TextHelper")
(rb/eval "PostsController.include ActionView::Context")
