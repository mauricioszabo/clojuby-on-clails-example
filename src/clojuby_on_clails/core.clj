(ns clojuby-on-clails.core
  (:require [clojuby.core :as rb :refer [ruby rb-require]])
            ; [clojuby-on-clails.controllers.posts-controller])
  (:gen-class))

(defn- start-server! []
  (rb-require "./blog-example/config/application.rb")
  (rb-require "./blog-example/config/environments/development.rb")
  (rb-require "./blog-example/config/environment.rb")
  (rb/eval "Rails.application.load_server")
  (def s (rb/eval "Rack::Server.new(app: Rails.application, Port: 3000)"))
  (def r (future (ruby (. s start))))
  (requiring-resolve 'clojuby-on-clails.controllers.posts-controller/PostsController))
#_(start-server!)

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))
